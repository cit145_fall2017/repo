/*
 Software Description: The black Jack software could be installed on any gambling machine.
 The software designed to play as one player against the machine.
 Its perfectly handles the calculation of player balance, bid options and machine balance.
 ......
 Developed by: The Coders 2016
 CCAC North Campus\ CIT-145 North \ Prof. Jeff Seamean
 */
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


struct card {
    int value;
    char symbol;
    char type[20];
};
struct player { // the struct for dealer and player
    char user_name[20];
    struct card cards[10];
    int user_id;
    int total_hnd_value;
    int cash;
    int bid;
};

// Declare the structs


struct player player_1;
struct player dealer;
struct card cards[52];

// Declare the methods

void print_graphic_card(struct card *card); // to display the cards in graphic mode.
void print_card(struct card c); // to print the information of cards
void seed_random(); // initialize the random number
int get_random_number(int decksize);  // get a random number within 0 to 51 range (array indx size of cards)
void insert_card_KQJA(int val, char suit, char* type, int idx); // to generate Ace , king , Queen , Jack cards, k for king q for queen etc
void generate_suit(int idx, char suit); // this method to generate suits from 2 to 9
void generate_cards(); // generate the full deck
void del_card_from_deck(int ran, int decksize); // this method to delete the card from the deck after the player or dealer call for card.
void insert_cash_into_machine(int cash); // minimum chips should be 1000
bool is_There_ace(struct card card);// check if there is Ace card to help us figure out what the Ace value should be
void change_ACE_value_to_one(struct card *c);
int calc_hand_value(struct player *p, int card_qty); // calc the player hand value and handle the ace value
void end_porgram(); // to end the program
void Player_1_bid(int bid);
void insert_cash_player_1(int cash); // insert cash into player balance
void logo(); // to print the logo

//=====================================================================
int main(void)
{
    logo();
    int cash;
    printf("Enter 1000 chips into machine to run it ");
    
    do {
        scanf("%d", &cash);
        if (cash >= 1000) {
            insert_cash_into_machine(cash);
            break;
        }
        printf("insufecient amount, you need to add at least 1000\n");
        fseek(stdin, 0, SEEK_END);
    } while (cash < 1000);
    //=====================================================================
    
    
    printf("Welcome To BlackJack In C..... Enter your username \n");
    fseek(stdin, 0, SEEK_END);
    scanf("%s", player_1.user_name);
    printf("Welcome %s, please insert cash (Multiple of 5) into machine: \n", player_1.user_name);
    cash = 0;
    fseek(stdin, 0, SEEK_END);
    scanf("%d", &cash);
    //=====================================================================
    
    
    while (1) {
        if (cash % 5 != 0 || cash <= 0 ) {
            printf("Please insert multiple of 5 ----- Minimum cash is 5\n");
            fseek(stdin, 0, SEEK_END);
            scanf("%d", &cash);
        }
        if (cash == 99) {
            printf("Please comeback soon\n");
            end_porgram();
        }
        else {
            insert_cash_player_1(cash);
            break;
        }
    }
    //=====================================================================
    
    
    seed_random();
    generate_cards();
    for (int i = 0; i < 4 * 13; i++) {
        print_card(cards[i]);
    }
    int decksize = 52;
    int start_or_bid = 0;
    printf("Printting cards ............ system check >>>>>>>>>>>>> \n");
    
    logo();
    
    while (start_or_bid != 99) {
        printf("Your Balance is %d\n", player_1.cash);
        printf("Current Machine Balance is %d \n", dealer.cash);
        while (player_1.cash <= 0) { //  if player balance is zero, the player can't play until insert cash
            cash = 0;
            printf("Your balance is zero \n");
            printf("Please insert multiple of 5 ----- Minimum cash is 5\n");
            fseek(stdin, 0, SEEK_END);
            scanf("%d", &cash);
            
            if (cash % 5 != 0 || cash == 0) {
                printf("Please insert multiple of 5 ----- Minimum cash is 5\n");
                fseek(stdin, 0, SEEK_END);
                scanf("%d", &cash);
            }
            if (cash % 5 == 0 && cash >0) {
                insert_cash_player_1(cash);
                break;
            }
            
        }
        
        printf("Your Balance is %d\n", player_1.cash);
        printf("Machine Balance is %d \n", dealer.cash);
        printf("Bid 5 , 10 ,15 or 99 to quit. \n");
        fseek(stdin, 0, SEEK_END);
        int ret;
        //=====================================================================
        while (1) { // incase the user enters a char instead of num
            ret = scanf("%d", &start_or_bid);
            if (ret == 1) break;
            else {
                printf("You have entered an invalid number\n");
                fseek(stdin, 0, SEEK_END);
            }
        }
        
        if (start_or_bid == 99) {
            printf("Your Balance is %d  ....... please comeback soon\n", player_1.cash);
            end_porgram();
        }
        else {
            while (start_or_bid > player_1.cash || start_or_bid > 15 || start_or_bid % 5 != 0|| start_or_bid == 0) {
                printf("you'r trying to bid higher than your balance or entered invalid number \n");
                printf("Bid 5 10 15 \n");
                fseek(stdin, 0, SEEK_END);
                while (1) { // incase the user enters a char instead of num
                    ret = scanf("%d", &start_or_bid);
                    if (ret == 1) break;
                    else {
                        printf("You have entered an invalid number\n");
                        fseek(stdin, 0, SEEK_END);
                    }
                }
                if (start_or_bid < player_1.cash) {
                    if (start_or_bid == 5 || start_or_bid == 10 || start_or_bid == 15) {
                        break;
                    }
                    else {
                        printf("bid 5 10 15 \n");
                        fseek(stdin, 0, SEEK_END);
                        while (1) { // incase the user enters a char instead of num
                            ret = scanf("%d", &start_or_bid);
                            if (ret == 1) break;
                            else {
                                printf("You have entered an invalid number\n");
                                fseek(stdin, 0, SEEK_END);
                            }
                        }
                        
                    }
                }
            }
            Player_1_bid(start_or_bid);  // if the user didn't imput 5 , 10 , 15 the default bid will be 5
            printf("your current bid is %d\n",start_or_bid);
        }
        int card_qty = 0;
        while (1) {
            printf("Hi %s ,please enter 1 to call for card or 2 to hold 99 quit", player_1.user_name);
            fseek(stdin, 0, SEEK_END);
            start_or_bid = -1;
            scanf("%d", &start_or_bid);
            if (start_or_bid == 99) {
                printf("your current Balance is %d , please comeback soon \n",player_1.bid);
                end_porgram();
            }
            if (start_or_bid == 1) { // if player one call for a card
                int ran = get_random_number(decksize);
                print_graphic_card(&(cards[ran]));
                player_1.cards[card_qty++] = cards[ran];
                player_1.total_hnd_value = calc_hand_value(&player_1, card_qty);
                del_card_from_deck(ran, decksize);
                decksize = decksize - 1;
                printf("your hand is  ");
                print_card(player_1.cards[card_qty - 1]);
                if (player_1.total_hnd_value < 21) {
                    printf("your currnet hand value %d \n", player_1.total_hnd_value);
                }
                else if (player_1.total_hnd_value == 21) {
                    start_or_bid = 2;
                    printf("You Have Black Jack, wait for Dealer to play \n");
                }
                else {
                    printf("sorry you have lost with current hand %d\n", player_1.total_hnd_value);
                    dealer.cash += player_1.bid; // the player bid is already deducted from the player balance
                    generate_cards(); // it's like return cards to the deck
                    dealer.total_hnd_value = 0;
                    player_1.total_hnd_value = 0;
                    card_qty = 0; // reset player hand qty
                    decksize = 52; // reset deck size
                    break;
                }
            }
            if (start_or_bid == 2) {
                printf("Now.... It's the dealer turn\n");
                card_qty = 0; // reset player hand qty
                dealer.total_hnd_value = calc_hand_value(&dealer, card_qty);
                while (dealer.total_hnd_value < player_1.total_hnd_value) {
                    int ran = get_random_number(decksize);
                    print_graphic_card(&(cards[ran]));
                    dealer.cards[card_qty++] = cards[ran];
                    dealer.total_hnd_value = calc_hand_value(&dealer, card_qty);
                    del_card_from_deck(ran, decksize);
                    decksize = decksize - 1;
                    printf("dealer have ");
                    print_card(dealer.cards[card_qty - 1]);
                    printf("dealer current total hand %d\n", dealer.total_hnd_value);
                }
                if (dealer.total_hnd_value >= player_1.total_hnd_value && dealer.total_hnd_value <= 21) {
                    printf("sorry you lost\n");
                    dealer.cash += player_1.bid; // the player bid is already deducted from the player balance
                    generate_cards();
                    dealer.total_hnd_value = 0;
                    player_1.total_hnd_value = 0;
                    card_qty = 0; // reset player hand qty
                    decksize = 52; // reset deck size
                    break;
                }
                else if (dealer.total_hnd_value > 21) {
                    printf("you won\n");
                    dealer.cash -= player_1.bid;//the dealer does not have a method for bid it just deduct directly from dealer or machine cash  based on player bid
                    player_1.cash += (player_1.bid * 2);
                    generate_cards();
                    dealer.total_hnd_value = 0;
                    player_1.total_hnd_value = 0;
                    card_qty = 0; // reset player hand qty
                    decksize = 52; // reset deck size
                    break;
                }
            }
        }
    }
    system("pause");
    return EXIT_SUCCESS;
}


void print_graphic_card(struct card *card) // to show the card in graphic mode.
{
    char* heart = "H";
    char* spade = "S";
    char* club = "C";
    char* diamond = "D";
    switch (card->type[0]) {
        case '2':
            printf(" -------\n");
            printf("|2      |\n");
            printf("|   %c   |\n", card->symbol);
            printf("|      2|\n");
            printf(" -------\n");
            break;
        case '3':
            printf(" -------\n");
            printf("|3      |\n");
            printf("|   %c   |\n", card->symbol);
            printf("|      3|\n");
            printf(" -------\n");
            break;
            
        case '4':
            printf(" -------\n");
            printf("|4      |\n");
            printf("|   %c   |\n", card->symbol);
            printf("|      4|\n");
            printf(" -------\n");
            break;
        case '5':
            printf(" -------\n");
            printf("|5      |\n");
            printf("|   %c   |\n", card->symbol);
            printf("|      5|\n");
            printf(" -------\n");
            break;
        case '6':
            printf(" -------\n");
            printf("|6      |\n");
            printf("|   %c   |\n", card->symbol);
            printf("|      6|\n");
            printf(" -------\n");
            break;
            
        case '7':
            printf(" -------\n");
            printf("|7      |\n");
            printf("|   %c   |\n", card->symbol);
            printf("|      7|\n");
            printf(" -------\n");
            break;
            
        case '8':
            printf(" -------\n");
            printf("|8      |\n");
            printf("|   %c   |\n", card->symbol);
            printf("|      8|\n");
            printf(" -------\n");
            break;
            
        case '9':
            printf(" -------\n");
            printf("|9      |\n");
            printf("|   %c   |\n", card->symbol);
            printf("|      9|\n");
            printf(" -------\n");
            break;
            
        case '1':
            printf(" -------\n");
            printf("|10     |\n");
            printf("|   %c   |\n", card->symbol);
            printf("|     10|\n");
            printf(" -------\n");
            break;
        case 'J':
            printf(" -------\n");
            printf("|J      |\n");
            printf("|   %c   |\n", card->symbol);
            printf("|      J|\n");
            printf(" -------\n");
            break;
        case 'Q':
            printf(" -------\n");
            printf("|Q      |\n");
            printf("|   %c   |\n", card->symbol);
            printf("|      Q|\n");
            printf(" -------\n");
            break;
            
        case 'K':
            printf(" -------\n");
            printf("|K      |\n");
            printf("|   %c   |\n", card->symbol);
            printf("|      K|\n");
            printf(" -------\n");
            break;
            
            
        case 'A':
            printf(" -------\n");
            printf("|A      |\n");
            printf("|   %c   |\n", card->symbol);
            printf("|      A|\n");
            printf(" -------\n");
            break;
    }
}



void print_card(struct card c) {
    printf("Card [type=%s, symbol=%c] value=%d\n", c.type, c.symbol, c.value);
}
void seed_random() {// initialize the random number
    srand(time(NULL));
}
int get_random_number(int decksize) { // get a random number within 0 to 51 range (array indx size of cards)
    return rand() % decksize;
}
void insert_card_KQJA(int val, char suit, char* type, int idx) {// k for king q for queen etc
    cards[idx].value = val;
    cards[idx].symbol = suit;
    strcpy(cards[idx].type, type);
}
void generate_suit(int idx, char suit) { // this method to generate suits from 2 to 9 
    for (int i = 0; i < 9; i++) {
        cards[idx + i].value = i + 2;
        cards[idx + i].symbol = suit;
        sprintf(cards[idx + i].type, "%d", cards[idx + i].value);
    }
    insert_card_KQJA(10, suit, "Jack", idx + 9);  // then we will generate the pictures cards
    insert_card_KQJA(10, suit, "Queen", idx + 10);
    insert_card_KQJA(10, suit, "King", idx + 11);
    insert_card_KQJA(11, suit, "Ace", idx + 12);
}
void generate_cards() { // generate the the full deck
    generate_suit(0, 'H');
    generate_suit(13, 'S');
    generate_suit(26, 'C');
    generate_suit(39, 'D');
}

void del_card_from_deck(int ran, int decksize) { // this method to delete the card from the deck after the player or dealer call it 
    for (int i = ran; i < decksize - 1; i++)
    {
        cards[i] = cards[i + 1];
    }
}

void insert_cash_into_machine(int cash) { // minimum chips should be 1000
    dealer.cash = cash;
}

bool is_There_ace(struct card card) { // check if there is Ace card in order to help us figure out what the Ace value should be 
    if (strcmp(card.type, "Ace") == 0)
        return true;
    return false;
}
void change_ACE_value_to_one(struct card *c) {
    c->value = 1;// pointer
}

int calc_hand_value(struct player *p, int card_qty) { // calc the player hand value and handle the ace value
    int i;
    int num_aces = 0;
    int num_other_cards = 0;
    int value = 0;
    for (i = 0; i < card_qty; i++) {
        if (is_There_ace(p->cards[i])) { // count how many aces in the hand
            num_aces++;
            value += 1;
        }
        else {
            num_other_cards++;
            value += p->cards[i].value;
        }
    }
    // blackjack!
    if (num_aces == 2 && num_other_cards == 0) { // if we have the first 2 cards are aces, then it's BJ
        return 21;
    }
    if (num_aces > 0 && value <= 11) {
        value += 10;
    }
    return value;
}
void end_porgram()
{
    system("pause");
    exit(0);
    //return EXIT_SUCCESS;
}
void Player_1_bid(int bid) {
    switch (bid)
    {
        case 5:
            player_1.bid = 5;
            player_1.cash -= 5; //  deduct 5$ from player balance 
            break;
        case 10:
            player_1.bid = 10;
            player_1.cash -= 10;
            break;
        case 15:
            player_1.bid = 15;
            player_1.cash -= 15;
            break;
    }
}

void insert_cash_player_1(int cash) {
    player_1.cash = cash;
}

void logo() {
    
    {
        printf(
               
               ".______    __          ___       ______  __  ___        __       ___       ______  __  ___\n"
               "|   _  \\  |  |        /   \\     /      ||  |/  /       |  |     /   \\     /      ||  |/  /\n"
               "|  |_)  | |  |       /  ^  \\   |  ,----'|  '  /        |  |    /  ^  \\   |  ,----'|  '  /\n"
               "|   _  <  |  |      /  /_\\  \\  |  |     |    <   .--.  |  |   /  /_\\  \\  |  |     |    <\n"
               "|  |_)  | |  `----./  _____  \\ |  `----.|  . \\  |  `--'   |  /  _____  \\ |  `----.|  .  \\\n"
               "|______/  |_______/__/     \__ \\ \\______||__|\__\\  \\_______/  /__/      \__\\ \\______||__|\\__\\\n"
               );
        printf("\n");
    }
    
}

